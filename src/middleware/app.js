import { appActions } from '../actions';

export default function appMiddleware({ dispatch, getState }) {
  return next => (action) => {
    switch (action.type) {
      case appActions.LOG:
        return Promise.resolve(false);
        // break;
      default:
        return next(action);
    }
  };
}
