import { combineReducers } from 'redux';
import intl from './intl';

export default combineReducers({
  intl,
});
