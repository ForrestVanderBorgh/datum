export const messages = {
  subscription: 'Subscription',
  intro: 'Edit {filePath} and save to reload.',
  signOut: 'Sign Out',
  'intro.header': 'Example Intro',
  'login.header': 'Example Header',
};
