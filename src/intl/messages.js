import { defineMessages } from 'react-intl';

// Put strings/messages here if they're used in multiple components
// TODO: Export helper function to merge local messages with these?

const messages = defineMessages({
  subscription: {
    id: 'subscription',
    defaultMessage: 'Subscription',
    description: 'Refers to Up spend options',
  },
  intro: {
    id: 'intro',
    defaultMessage: 'Edit {filePath} and save to reload.',
    description: 'Instructions on how to edit page',
  },
  signOut: {
    id: 'signOut',
    defaultMessage: 'Sign Out',
  },
});

// TODO: potentially different groups/namespaces of messages to pass around here
export default messages;
