const actions = {
  LOG: 'LOG_STACKDRIVER',
};

export function log(channel, level, message) {
  return { type: actions.LOG, channel, level, message };
}

export default actions;
