const actions = {
  UPDATE: 'INTL_UPDATE',
};

export function updateIntl({ locale, messages }) {
  return {
    type: actions.UPDATE,
    payload: { locale, messages },
  };
}

export default actions;
