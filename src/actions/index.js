import { bindActionCreators } from 'redux';
import appConst, * as appActions from './app';
import intlConst, * as intlActions from './intl';

// allow middleware and reducers to import specific action constants from this index
export { appConst as appActions };
export { intlConst as intlActions };

// by default, export a function to map all actions bound to dispatch
// for use when connecting to redux via redux-wrapper
export default function actions(dispatch) {
  return {
    appActions: bindActionCreators({ ...appActions }, dispatch),
    intlActions: bindActionCreators({ ...intlActions }, dispatch),
  };
}
