import React from 'react';
import ReactDOM from 'react-dom';
import { injectProviders } from './components';
import Routes from './routes';
// import { registerServiceWorker } from './utils';
import './index.css';

const Injected = injectProviders(Routes);

ReactDOM.render(
  <Injected />,
  document.getElementById('root'),
);