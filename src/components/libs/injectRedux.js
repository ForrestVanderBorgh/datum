import { connect } from 'react-redux';
import actions from '../../actions';
import rootReducer from '../../reducers';

export default function injectRedux(InnerComponent, stateToProps = null) {
  return connect(stateToProps || rootReducer, actions)(InnerComponent);
}
