import React from 'react';

// Using function to wrap div instead of class benefits from more RN optimizations, see:
// https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-stateless-function.md
export default function View(props) {
  return (
    <div {...props} />
  );
}
