import React from 'react';
import { addLocaleData, IntlProvider } from 'react-intl';
import en from 'react-intl/locale-data/en';
import injectRedux from './injectRedux';

addLocaleData([...en]);

function defaultSelector(state) {
  return state.intl;
}

function intlStateToProps(state, { intlSelector = defaultSelector }) {
  const intl = intlSelector(state);
  return {
    ...intl,
    key: intl.locale,
  };
}

const ConnectedIntlProvider = injectRedux(IntlProvider, intlStateToProps);

export default function injectIntlProvider(InnerComponent) {
  return (props) => {
    return (
      <ConnectedIntlProvider>
        <InnerComponent />
      </ConnectedIntlProvider>
    );
  };
}
