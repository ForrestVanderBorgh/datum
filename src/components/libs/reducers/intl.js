import { intlActions } from '../actions';

import { messages } from '../intl/lang/en-US';

const initialState = {
  locale: 'en',
  messages: {
    ...(messages || {}),
  },
};

export default function intl(state = initialState, action) {
  switch (action.type) {
    case intlActions.UPDATE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
