import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

injectTapEventPlugin();

export default function injectMuiThemeProvider(InnerComponent) {
  return (props) => {
    return (
      <MuiThemeProvider>
        <InnerComponent />
      </MuiThemeProvider>
    );
  };
}
