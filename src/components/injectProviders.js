import {
  injectIntlProvider,
  injectMuiThemeProvider,
  injectReduxProvider,
} from './libs';

export default function injectProviders(InnerComponent, initialState = null) {
  return injectReduxProvider(
    injectIntlProvider(
      injectMuiThemeProvider(InnerComponent),
    ),
    initialState,
  );
}
