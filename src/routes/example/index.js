import React, { Component } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import { View, injectRedux } from '../../components/libs';
import './test.css';
import { Route } from 'react-router-dom';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { orange500, blue500, grey500 } from 'material-ui/styles/colors';

import Parallax from 'react-simple-parallax';

class Example extends Component {
  constructor(props) {
    super(props);

  }

  render () {
    return (
        <section>
          	<View className="SomeView">
          		<div>Hello World!</div>
          	</View>
        </section>
    );
  }
}

export default injectRedux(injectIntl(Example));

