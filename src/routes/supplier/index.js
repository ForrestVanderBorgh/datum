import React, { Component } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import { View, injectRedux } from '../../components/libs';

import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import './supplier.css';

class Supplier extends Component {
  constructor(props) {
    super(props);
    this.state = {
      targetRender: 'Quote Feed',
    };
    this._appNav = this._appNav.bind(this);
  }

  _appNav(evt, targetRender) {
    this.setState({targetRender: targetRender});
  }

// Todo: Convert cards into higher order component. after fixing styling. One for each card component.
  render() {
    console.log(this.state);
    return (
     <View className="App">
        <View className="App-header">
          <h2>{this.state.targetRender}</h2>
        </View>
        <View className='body'>
          <View className='navigation'>
            <FlatButton
            className='navigationFlatButton'
            label="Quote Feed" 
            onClick={ evt => this.setState({targetRender: 'Quote Feed'})}
            fullWidth={true}
            /> <br />
            <FlatButton 
            className='navigationFlatButton'
            label="Submitted Bids"
            onClick={ evt => this.setState({targetRender: 'Submitted Bids'})}
            fullWidth={true}
            /> <br />
            <FlatButton 
            className='navigationFlatButton'
            label="Purchase Orders"
            onClick={ evt => this.setState({targetRender: 'Purchase Orders'})}
            fullWidth={true}
            /> <br />
          </View>
          <View>
            <Card className='container'>
              <CardHeader
                className='rfqTitle'
                title="RFQ #1000"
                actAsExpander={true}
                showExpandableButton={true}
              />
              <CardText 
              expandable={true}
              >
                <View className='cardTextContainer'>
                  <p className='cardText'>
                    Part Number:<br /> Quantity Required:<br /> Need Date:<br />
                  </p>

                  <p className='cardText'>
                    38497234-001<br /> 10,000
                    <br /> October 20, 2017<br />
                  </p>

                  <p className='cardText'>
                    Bids: 3 <br />
                  </p>
                </View>
              </CardText>
            </Card>      
          </View>
        </View>
      </View>
    );
  }
}

export default injectRedux(injectIntl(Supplier));

