import React, { Component } from 'react';
// import queryString from 'query-string';
import { injectRedux } from '../components/libs';

export default function injectAuthRequired(InnerComponent) {
  return injectRedux(
    class AuthWrapper extends Component {
      constructor(props) {
        super(props);

        this.state = {
          authed: false,
        };
      }

      componentWillMount() {
       // const { history, location: { pathname, search } } = this.props;
       // const params = queryString.parse(search);
       // if (params.someToken) {
          this.setState({ authed: true });
      // }
      }

      render() {
        return this.state.authed ? (<InnerComponent {...this.props} />) : (<div />);
      }
    },
  );
}
