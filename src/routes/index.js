import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import injectAuthRequired from './injectAuthRequired';
import Home from './home';
import Buyer from './buyer';
import Login from './login';
import Supplier from './supplier';
import Register from './register';
import Example from './example';

export default function Router(props) {
  return (
    <BrowserRouter>
      <Switch>
        <Route
          exact
          path='/'
          component={injectAuthRequired(Home)}
        />
        <Route
          exact
          path='/login'
          component={injectAuthRequired(Login)}
        />
        <Route
          exact
          path='/buyer'
          component={injectAuthRequired(Buyer)}
        />
        <Route
          exact
          path='/supplier'
          component={injectAuthRequired(Supplier)}
        />
        <Route
          exact
          path='/register'
          component={injectAuthRequired(Register)}
        />
        <Route
          exact
          path='/example'
          component={injectAuthRequired(Example)}
        />
      </Switch>
    </BrowserRouter>
  );
}
