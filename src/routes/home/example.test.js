/* global it */
import React from 'react';
import ReactDOM from 'react-dom';
import Payment from './index';
import { injectProviders } from '../../components';

// TODO populate initialState with dummy data
const Injected = injectProviders(Payment);

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <Injected />,
    div,
  );
});
