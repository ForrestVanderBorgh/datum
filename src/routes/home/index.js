import React, { Component } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import { View, injectRedux } from '../../components/libs';
import fire from '../../utils/fire';

import logo from './content/ColourBlack.png';
import content1 from './content/1.jpg';
import slide1 from './content/Connections.png';
import slide2 from './content/rfq.png';
import slide3 from './content/Dashboard.png';

import './home.css';
import { Route } from 'react-router-dom';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { orange500, blue500, grey300, grey20 } from 'material-ui/styles/colors';

import Parallax from 'react-simple-parallax';


const styles = {
  loginButton: {
    margin: 12
  },
  registerButton: {
    width: '50%'
  },
  headerButton: {
    color: grey20,
  },
  fieldUnderline: {
    borderColor: '#1B75BC'
  },
  fieldLabel: {
    color: '#1B75BC'
  }

};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dev: 'true',
      source: 1,
      filename: 'slide1',
      submitted: false,
      email: ''
    }
    this._onLogIn = this._onLogIn.bind(this);
    this._register = this._register.bind(this);
    this._renderAlphaSite = this._renderAlphaSite.bind(this);
    this._onSlideClick = this._onSlideClick.bind(this);
    this._renderSlide = this._renderSlide.bind(this);
    this._renderRegInput = this._renderRegInput.bind(this);
    this._renderConfirmation = this._renderConfirmation.bind(this);
    this._submitEmail = this._submitEmail.bind(this);
    this._validateEmail = this._validateEmail.bind(this);
  }


  _register() {
    return (
      window.location = '/register'
    );
  }

  _onLogIn() {
    return (
      window.location = '/login'
    );
  }

  _onSlideClick() {
    const self = this;
    if (this.state.source === 3) {
      self.setState({source: 1});
    } else {
      const i = this.state.source + 1;
      this.setState({source: i})
    }

  }

  _renderSlide() {
    return (
      <View className="SlideRowContainer">
        <View id="SlideRow">
          <View id="RowText">
          Find and connect to proven top quality suppliers
          </View>
          <img src={slide1} id="Slide" />
        </View>
        <View id="SlideRow">
          <View id="RowText">
          Track and manage orders
          </View>
          <img src={slide2} id="Slide" />
        </View>
        <View id="SlideRow">
          <View id="RowText">
          Steer your business with data
          </View>
          <img src={slide3} id="Slide" />
        </View>
      </View>
    );
  }

  _renderRegInput(buttonText) {
    return (
      <TextField
        floatingLabelText={buttonText}
        floatingLabelStyle={styles.floatingLabelStyle}
        floatingLabelFocusStyle={styles.fieldLabel}
        underlineFocusStyle={styles.fieldUnderline}

        errorText={this.state.errorText}
        className="RegisterationTextField"
        onChange={(evt, newValue) => {
          switch (buttonText) {
            // case "Name":
            //   this.setState({name: newValue})
            //   break;
            // case "Company":
            //   this.setState({company: newValue})
            //   break;
            case "Email":
              this.setState({email: newValue})
            //   break;
            // case "Notes":
            //   this.setState({notes: newValue})
            //   break;
          }
        }}
      >
      </TextField>
    )
  }

  _validateEmail(emailInput) {
    const emailAddress = emailInput;
    const atPos = emailAddress.indexOf("@");
    const dotPos = emailAddress.lastIndexOf(".");
    if (atPos<1 || dotPos<atPos+2 || dotPos+2>=emailAddress.length) {
      this.setState({errorText: 'Please check your submitted address'});
      return false;
    } else {
      return true;
    }

  }
  
  _submitEmail() {
    if (this._validateEmail(this.state.email)) {
      this.setState({submitted: true})
      fire.database().ref('emailAddresses').push(this.state.email)
    } else {
      this.setState({errorText: 'Please check your email'})
    }
  }

  _renderConfirmation() {
    const self = this;
    if (!this.state.submitted) {
      return (
        <View className="ConfirmationContainer">
          {this._renderRegInput('Email')}
          <FlatButton
          label="Register"
          labelStyle={styles.headerButton}
          style={styles.registerButton}
          backgroundColor="grey20"
          onClick={this._submitEmail}
          >
          </FlatButton>
        </View>
      )
    } else {
      return (
        <View className="RegisterConfirmText">
          Thank you for registering
        </View>
      )
    }
   
  }

  _renderAlphaSite() {
    return (
      <View className="HomeWrapper">
        <View className="ToolbarHeader" >
          <View className="LogoContainer">
            <img src={logo} className="Logo" alt="logo" />
          </View>
        </View>
        <View className="Banner">
          <View className="ImageContainer">
            <img className="Image" />
          </View>
          <View className="ContentHead">
            <View className="Content">
              <View className="Intro">
                Allow us to introduce ourselves...
              </View>
              <View className="SlideshowContainer" >
                <View className= "Slideshow">
                  {this._renderSlide()}
                </View>
              </View>
              <View className="RegisterContainer">
                <View className="RegisterText">
                We're working hard to bring you datum soon! If you'd like to receive an
                email when we're ready, we'd love to notify you!
                </View>
               {this._renderConfirmation()}
              </View>
              <View className="BGImage">
              </View>
            </View>
          </View>
        </View>

        <View className="Footer">
        </View>
      </View>
    );
  }

  render() {
    if (this.state.dev) {
      return this._renderAlphaSite();
    }
    return (
      <View className="HomeWrapper">
        <View className="ToolbarHeader" >
          <View className="LogoContainer">
            <img src={logo} className="Logo" alt="logo" />
          </View>
          <View className="Links">
            <FlatButton
              label="Log in"
              labelStyle={styles.headerButton}
              style={styles.loginButton}
              onClick={this._onLogIn}
              >
            </FlatButton>
            <FlatButton
              label="Register"
              labelStyle={styles.headerButton}
              style={styles.registerButton}
              onClick={this._register}
              >
            </FlatButton>
          </View>
        </View>
        <View className="Banner">
          <View className="ImageContainer">
            <img className="Image" />
          </View>
          <View className="ContentHead">
            <a>
              Marketing Material goes here.
              </a>
          </View>
          <View className="Content">
            <a>
              bla
            </a>
          </View>
        </View>
        <View className="Filler">
        </View>
        <View className="Footer">
        </View>
      </View>

    );
  }
}

export default injectRedux(injectIntl(Home));
