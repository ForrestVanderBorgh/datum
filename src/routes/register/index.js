import React, { Component } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import { View, injectRedux } from '../../components/libs';
import './register.css';
import { Route } from 'react-router-dom';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { orange500, blue500, grey500 } from 'material-ui/styles/colors';

const styles = {
  errorStyle: {
    color: orange500,
  },
  underlineStyle: {
    borderColor: orange500,
  },
  floatingLabelStyle: {
    color: grey500
    ,
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
  loginButton: {
    margin: 12
  },
};


class Register extends Component {
  constructor(props) {
    super(props);
    this._renderTextField = this._renderTextField.bind(this);
    this._register = this._register.bind(this);

  }

  _register() {
    window.location = '/'
  }

  _renderTextField(textFieldLabel) {
    return (
      <TextField
          floatingLabelText={textFieldLabel}
          floatingLabelStyle={styles.floatingLabelStyle}
          floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
          errorText=''
          className="AuthTextField"
        >
        </TextField>
    )
  }

  render() {
    return (
      <View className="App">
        <View className="RegistrationForm">
          {this._renderTextField("Company Name")}
          {this._renderTextField("Address 1")}
          {this._renderTextField("Address 2")}
          {this._renderTextField("City")}
          {this._renderTextField("State/Province")}
          {this._renderTextField("Zip/Postal Code")}
          {this._renderTextField("Country")}
          {this._renderTextField("Company EIN")}
          {this._renderTextField("Email")}
          <RaisedButton
            label="Register"
            style={styles.loginButton}
            onClick={this._register}
            >
          </RaisedButton>
        </View>
      </View>
    );
  }
}

export default injectRedux(injectIntl(Register));

