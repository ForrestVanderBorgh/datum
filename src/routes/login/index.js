import React, { Component } from 'react';
import { defineMessages, injectIntl, FormattedMessage } from 'react-intl';
import { View, injectRedux } from '../../components/libs';
import logo from './2.png';
import './login.css';
import { Route } from 'react-router-dom';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { orange500, blue500, grey500 } from 'material-ui/styles/colors';

const styles = {
  errorStyle: {
    color: orange500,
  },
  underlineStyle: {
    borderColor: orange500,
  },
  floatingLabelStyle: {
    color: grey500
    ,
  },
  floatingLabelFocusStyle: {
    color: blue500,
  },
  loginButton: {
    margin: 12
  }
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      auth: false,
      errorText: ''
    }

    this.renderInput = this.renderInput.bind(this);
    this._onLogIn = this._onLogIn.bind(this);
    this._register = this._register.bind(this);

  }

  _onLogIn() {
    const {
      username,
      password
    } = this.state;

    if (username === 'buyer') {
      return (
        window.location = '/buyer'
      );
    } else if (username === 'supplier') {
      return (
        window.location = '/supplier'
      )
    };
    this.setState({errorText: 'Invalid username or password'});
  }

  _register() {
    return (
      window.location = '/register'
    );
  }
  //onClick={() => { history.push(pathName) }} 
  renderInput(buttonText, pathName) {
    const self = this;
    const inputType = (buttonText === 'Email') ? 'text' : 'password';
    const stateKey = (buttonText === 'Email') ? 'username' : 'password';
    return (
      <TextField
        floatingLabelText={buttonText}
        floatingLabelStyle={styles.floatingLabelStyle}
        floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
        errorText={this.state.errorText}
        className="AuthTextField"
        type={inputType}
        onChange={(evt, newValue) => {
          if (stateKey === 'username') {
            self.setState({ username: newValue })
          } else {
            self.setState({ password: newValue })
          }
        }}
      >
      </TextField>
    )
  }

  render() {
    console.log('STATE', this.state);
    return (
      <View className="Wrapper">
        <View className="Header">
          <img src={logo} className="LoginLogo" alt="logo" />
        </View>
        <View className="AuthButtons">
          {this.renderInput('Email', '/supplier')} <br />
          {this.renderInput('Password', '/buyer')} <br />
            <RaisedButton
              label="Log in"
              style={styles.loginButton}
              onClick={this._onLogIn}
              >
            </RaisedButton>
            <RaisedButton
              label="Create Account"
              style={styles.loginButton}
              onClick={this._register}
              >
            </RaisedButton>
        </View>
      </View>

    );
  }
}

export default injectRedux(injectIntl(Login));
